# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Sergiu Bivol <sergiu@cip.md>, 2015, 2020, 2021, 2022.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-17 00:48+0000\n"
"PO-Revision-Date: 2022-04-04 10:11+0100\n"
"Last-Translator: Sergiu Bivol <sergiu@cip.md>\n"
"Language-Team: Romanian <kde-i18n-ro@kde.org>\n"
"Language: ro\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n==1 ? 0 : (n==0 || (n%100 > 0 && n%100 < "
"20)) ? 1 : 2;\n"
"X-Generator: Lokalize 21.12.3\n"

#. i18n: ectx: label, entry (cursorTheme), group (Mouse)
#: cursorthemesettings.kcfg:9
#, kde-format
msgid "Name of the current cursor theme"
msgstr "Denumirea tematicii de cursori actuale"

#. i18n: ectx: label, entry (cursorSize), group (Mouse)
#: cursorthemesettings.kcfg:13
#, kde-format
msgid "Current cursor size"
msgstr "Dimensiune actuală cursor"

#: kcmcursortheme.cpp:302
#, kde-format
msgid ""
"You have to restart the Plasma session for these changes to take effect."
msgstr ""
"Trebuie să reporniți sesiunea Plasma pentru ca modificările să intre în "
"vigoare."

#: kcmcursortheme.cpp:376
#, kde-format
msgid "Unable to create a temporary file."
msgstr "Nu s-a putut crea un fișier temporar."

#: kcmcursortheme.cpp:387
#, kde-format
msgid "Unable to download the icon theme archive: %1"
msgstr "Nu s-a putut descărca arhiva cu tematici de pictograme: %1"

#: kcmcursortheme.cpp:418
#, kde-format
msgid "The file is not a valid icon theme archive."
msgstr "Fișierul nu este o arhivă validă cu tematici de pictograme."

#: kcmcursortheme.cpp:425
#, kde-format
msgid "Failed to create 'icons' folder."
msgstr "Eșec la crearea dosarului „icons”."

#: kcmcursortheme.cpp:434
#, kde-format
msgid ""
"A theme named %1 already exists in your icon theme folder. Do you want "
"replace it with this one?"
msgstr ""
"O tematică cu denumirea %1 există deja în dosarul cu tematici de pictograme. "
"Doriți s-o înlocuiți cu aceasta?"

#: kcmcursortheme.cpp:438
#, kde-format
msgid "Overwrite Theme?"
msgstr "Suprascrieți tema?"

#: kcmcursortheme.cpp:462
#, kde-format
msgid "Theme installed successfully."
msgstr "Tematica a fost instalată cu succes."

#: package/contents/ui/Delegate.qml:54
#, kde-format
msgid "Remove Theme"
msgstr "Elimină tematica"

#: package/contents/ui/Delegate.qml:61
#, kde-format
msgid "Restore Cursor Theme"
msgstr "Restabilește tematica de cursori"

#: package/contents/ui/main.qml:20
#, kde-format
msgid "This module lets you choose the mouse cursor theme."
msgstr "Acest modul vă permite să alegeți tematica cursorului."

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Size:"
msgstr "Dimensiune:"

#: package/contents/ui/main.qml:134
#, kde-format
msgid "&Install from File…"
msgstr "&Instalează din fișier…"

#: package/contents/ui/main.qml:140
#, kde-format
msgid "&Get New Cursors…"
msgstr "&Obține cursori noi…"

#: package/contents/ui/main.qml:157
#, kde-format
msgid "Open Theme"
msgstr "Deschide tematica"

#: package/contents/ui/main.qml:159
#, kde-format
msgid "Cursor Theme Files (*.tar.gz *.tar.bz2)"
msgstr "Fișiere cu tematici de cursori (*.tar.gz *.tar.bz2)"

#: plasma-apply-cursortheme.cpp:42
#, kde-format
msgid "The requested size '%1' is not available, using %2 instead."
msgstr "Dimensiunea cerută „%1” nu e disponibilă, se folosește %2 în loc."

#: plasma-apply-cursortheme.cpp:63
#, kde-format
msgid ""
"This tool allows you to set the mouse cursor theme for the current Plasma "
"session, without accidentally setting it to one that is either not "
"available, or which is already set."
msgstr ""
"Această unealtă vă permite să stabiliți tematica cursorului mausului pentru "
"sesiunea Plasma actuală, fără a o stabili din greșeală la una indisponibilă "
"sau care e deja stabilită."

#: plasma-apply-cursortheme.cpp:67
#, kde-format
msgid ""
"The name of the cursor theme you wish to set for your current Plasma session "
"(passing a full path will only use the last part of the path)"
msgstr ""
"Denumirea tematicii pentru cursor pe care doriți s-o stabiliți pentru "
"sesiunea Plasma actuală (transmiterea unei căi complete va duce la folosirea "
"ultimei componente a căii)"

#: plasma-apply-cursortheme.cpp:68
#, kde-format
msgid ""
"Show all the themes available on the system (and which is the current theme)"
msgstr "Arată toate tematicile disponibile în sistem (și care e cea actuală)"

#: plasma-apply-cursortheme.cpp:69
#, kde-format
msgid "Use a specific size, rather than the theme default size"
msgstr ""
"Folosește o anumită dimensiune în loc de dimensiunea implicită a tematicii"

#: plasma-apply-cursortheme.cpp:90
#, kde-format
msgid ""
"The requested theme \"%1\" is already set as the theme for the current "
"Plasma session."
msgstr ""
"Tematica cerută „%1” e stabilită deja ca tematică pentru sesiunea Plasma "
"actuală."

#: plasma-apply-cursortheme.cpp:101
#, kde-format
msgid ""
"Successfully applied the mouse cursor theme %1 to your current Plasma session"
msgstr ""
"Tematica pentru cursorul mausului %1 a fost aplicată cu succes la sesiunea "
"Plasma actuală"

#: plasma-apply-cursortheme.cpp:103
#, kde-format
msgid ""
"You have to restart the Plasma session for your newly applied mouse cursor "
"theme to display correctly."
msgstr ""
"Trebuie să reporniți sesiunea Plasma pentru ca tematica pentru cursor "
"aplicată acum să fie afișată corect."

#: plasma-apply-cursortheme.cpp:113
#, kde-format
msgid ""
"Could not find theme \"%1\". The theme should be one of the following "
"options: %2"
msgstr ""
"Tematica „%1” nu a putut fi găsită. Tematica trebuie să fie una dintre "
"următoarele opțiuni: %2"

#: plasma-apply-cursortheme.cpp:121
#, kde-format
msgid "You have the following mouse cursor themes on your system:"
msgstr "Aveți următoarele tematici de cursori în sistem:"

#: plasma-apply-cursortheme.cpp:126
#, kde-format
msgid "(Current theme for this Plasma session)"
msgstr "(Tematica actuală pentru această sesiune Plasma)"

#: xcursor/xcursortheme.cpp:60
#, kde-format
msgctxt ""
"@info The argument is the list of available sizes (in pixel). Example: "
"'Available sizes: 24' or 'Available sizes: 24, 36, 48'"
msgid "(Available sizes: %1)"
msgstr "(Dimensiuni disponibile: %1)"

#~ msgid "Name"
#~ msgstr "Denumire"

#~ msgid "Description"
#~ msgstr "Descriere"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Sergiu Bivol"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "sergiu@cip.md"

#~ msgid "Cursors"
#~ msgstr "Cursori"

#~ msgid "(c) 2003-2007 Fredrik Höglund"
#~ msgstr "(c) 2003-2007 Fredrik Höglund"

#~ msgid "Fredrik Höglund"
#~ msgstr "Fredrik Höglund"

#~ msgid "Marco Martin"
#~ msgstr "Marco Martin"

#~ msgid "Confirmation"
#~ msgstr "Confirmare"

#~ msgctxt "@item:inlistbox size"
#~ msgid "Resolution dependent"
#~ msgstr "Dependent de rezoluție"

#~ msgid "Cursor Settings Changed"
#~ msgstr "Configurările cursorului s-au modificat"

#~ msgid "Drag or Type Theme URL"
#~ msgstr "Trageți sau scrieți URL-ul temei"

#~ msgid ""
#~ "Unable to download the cursor theme archive; please check that the "
#~ "address %1 is correct."
#~ msgstr ""
#~ "Nu s-a putut descărca arhiva temei de cursori. Verificați dacă adresa %1 "
#~ "este corectă."

#~ msgid "Get new color schemes from the Internet"
#~ msgstr "Preia scheme de culori noi din Internet"
