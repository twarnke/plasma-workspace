# translation of klipper.po to galician
# Galician translation of klipper.
# Copyright (C) 2000 Jesús Bravo Álvarez.
#
# Proxecto Trasno - Adaptación do software libre á lingua galega:  Se desexas
# colaborar connosco, podes atopar máis información en http://trasno.gpul.org
#
# First Version: 2000-11-04 14:02+0100
#
# Jesús Bravo Álvarez <jba@pobox.com>, 2000.
# Xabi García <xabigf@gmx.net>, 2003.
# Xabi G. Feal <xabigf@gmx.net>, 2006.
# mvillarino <mvillarino@users.sourceforge.net>, 2007, 2008, 2009.
# marce villarino <mvillarino@users.sourceforge.net>, 2009.
# Marce Villarino <mvillarino@kde-espana.es>, 2009, 2011.
# Marce Villarino <mvillarino@kde-espana.es>, 2012.
# Adrián Chaves Fernández <adriyetichaves@gmail.com>, 2015.
# Adrián Chaves (Gallaecio) <adrian@chaves.io>, 2017, 2018, 2019.
msgid ""
msgstr ""
"Project-Id-Version: klipper\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-12-19 00:48+0000\n"
"PO-Revision-Date: 2019-10-19 21:59+0200\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Galician <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.11.70\n"

#: configdialog.cpp:80
#, fuzzy, kde-format
#| msgid "Selection and Clipboard"
msgid "Selection and Clipboard:"
msgstr "Selección e portapapeis"

#: configdialog.cpp:87
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"When text or an area of the screen is highlighted with the mouse or "
"keyboard, this is the <emphasis>selection</emphasis>. It can be pasted using "
"the middle mouse button.<nl/><nl/>If the selection is explicitly copied "
"using a <interface>Copy</interface> or <interface>Cut</interface> action, it "
"is saved to the <emphasis>clipboard</emphasis>. It can be pasted using a "
"<interface>Paste</interface> action. <nl/><nl/>When turned on this option "
"keeps the selection and the clipboard the same, so that any selection is "
"immediately available to paste by any means. If it is turned off, the "
"selection may still be saved in the clipboard history (subject to the "
"options below), but it can only be pasted using the middle mouse button."
msgstr ""

#: configdialog.cpp:106
#, fuzzy, kde-format
#| msgid "Clipboard history"
msgid "Clipboard history:"
msgstr "Historial do portapapeis"

#: configdialog.cpp:112
#, fuzzy, kde-format
#| msgid " entry"
#| msgid_plural " entries"
msgctxt "Number of entries"
msgid " entry"
msgid_plural " entries"
msgstr[0] " entrada"
msgstr[1] " entradas"

#: configdialog.cpp:131 configdialog.cpp:169
#, fuzzy, kde-format
#| msgid "Replay action in history"
msgid "Always save in history"
msgstr "Reproducir a acción do historial"

#: configdialog.cpp:135
#, fuzzy, kde-format
#| msgid "Text selection only"
msgid "Text selection:"
msgstr "Só o texto da selección"

#: configdialog.cpp:137 configdialog.cpp:175
#, kde-format
msgid "Only when explicitly copied"
msgstr ""

#: configdialog.cpp:142
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid "Whether text selections are saved in the clipboard history."
msgstr "Está seguro de que quere eliminar todo o historial do portapapeis?"

#: configdialog.cpp:173
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Non-text selection:"
msgstr "Ignorar a selección"

#: configdialog.cpp:180
#, fuzzy, kde-format
#| msgid "Replay action in history"
msgid "Never save in history"
msgstr "Reproducir a acción do historial"

#: configdialog.cpp:185
#, kde-format
msgid ""
"Whether non-text selections (such as images) are saved in the clipboard "
"history."
msgstr ""

#: configdialog.cpp:250
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Show action popup menu:"
msgstr "Tempo de agarda para xanelas emerxentes de acción:"

#: configdialog.cpp:260
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When text that matches an action pattern is selected or is chosen from the "
"clipboard history, automatically show the popup menu with applicable "
"actions. If the automatic menu is turned off here, or it is not shown for an "
"excluded window, then it can be shown by using the <shortcut>%1</shortcut> "
"key shortcut."
msgstr ""

#: configdialog.cpp:269
#, kde-format
msgid "Exclude Windows..."
msgstr ""

#: configdialog.cpp:283
#, fuzzy, kde-format
#| msgid " second"
#| msgid_plural " seconds"
msgctxt "Unit of time"
msgid " second"
msgid_plural " seconds"
msgstr[0] " segundo"
msgstr[1] " segundos"

#: configdialog.cpp:284
#, kde-format
msgctxt "No timeout"
msgid "None"
msgstr ""

#: configdialog.cpp:293
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Options:"
msgstr "Accións"

#: configdialog.cpp:320
#, kde-format
msgid "Exclude Windows"
msgstr ""

#: configdialog.cpp:350
#, kde-kuit-format
msgctxt "@info"
msgid ""
"When a <interface>match pattern</interface> matches the clipboard contents, "
"its <interface>commands</interface> appear in the Klipper popup menu and can "
"be executed."
msgstr ""

#: configdialog.cpp:359
#, kde-format
msgctxt "@title:column"
msgid "Match pattern and commands"
msgstr ""

#: configdialog.cpp:359
#, fuzzy, kde-format
#| msgid "Description"
msgctxt "@title:column"
msgid "Description"
msgstr "Descrición"

#: configdialog.cpp:365
#, kde-format
msgid "Add Action..."
msgstr "Engadir unha acción…"

#: configdialog.cpp:369
#, kde-format
msgid "Edit Action..."
msgstr "Editar a acción…"

#: configdialog.cpp:374
#, kde-format
msgid "Delete Action"
msgstr "Eliminar a acción"

#: configdialog.cpp:381
#, kde-kuit-format
msgctxt "@info"
msgid ""
"These actions appear in the popup menu which can be configured on the "
"<interface>Action Menu</interface> page."
msgstr ""

#: configdialog.cpp:565
#, kde-kuit-format
msgctxt "@info"
msgid ""
"Delete the selected action <resource>%1</resource><nl/>and all of its "
"commands?"
msgstr ""

#: configdialog.cpp:566
#, fuzzy, kde-format
#| msgid "Delete Action"
msgid "Confirm Delete Action"
msgstr "Eliminar a acción"

#: configdialog.cpp:595
#, kde-format
msgctxt "General Config"
msgid "General"
msgstr "Xeral"

#: configdialog.cpp:595
#, kde-format
msgid "General Configuration"
msgstr "Configuración xeral"

#: configdialog.cpp:596
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgctxt "Popup Menu Config"
msgid "Action Menu"
msgstr "Accións"

#: configdialog.cpp:596
#, fuzzy, kde-format
#| msgctxt "Actions Config"
#| msgid "Actions"
msgid "Action Menu"
msgstr "Accións"

#: configdialog.cpp:597
#, fuzzy, kde-format
#| msgid "Actions Configuration"
msgctxt "Actions Config"
msgid "Actions Configuration"
msgstr "Configuración das accións"

#: configdialog.cpp:597
#, kde-format
msgid "Actions Configuration"
msgstr "Configuración das accións"

#: configdialog.cpp:600
#, kde-format
msgctxt "Shortcuts Config"
msgid "Shortcuts"
msgstr "Atallos"

#: configdialog.cpp:600
#, kde-format
msgid "Shortcuts Configuration"
msgstr "Configuración dos atallos"

#: configdialog.cpp:680
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The action popup will not be shown automatically for these windows, even if "
"it is enabled. This is because, for example, a web browser may highlight a "
"URL in the address bar while typing, so the menu would show for every "
"keystroke.<nl/><nl/>If the action menu appears unexpectedly when using a "
"particular application, then add it to this list. <link>How to find the name "
"to enter</link>."
msgstr ""

#: configdialog.cpp:693
#, kde-kuit-format
msgctxt "@info:tooltip"
msgid ""
"The name that needs to be entered here is the WM_CLASS name of the window to "
"be excluded. To find the WM_CLASS name for a window, in another terminal "
"window enter the command:<nl/><nl/>&nbsp;&nbsp;<icode>xprop | grep WM_CLASS</"
"icode><nl/><nl/>and click on the window that you want to exclude. The first "
"name that it displays after the equal sign is the one that you need to enter."
msgstr ""

#: editactiondialog.cpp:34 editcommanddialog.cpp:89
#, kde-format
msgid "Ignore"
msgstr "Ignorar"

#: editactiondialog.cpp:36
#, kde-format
msgid "Replace Clipboard"
msgstr "Substituír o portapapeis"

#: editactiondialog.cpp:38
#, kde-format
msgid "Add to Clipboard"
msgstr "Engadir ao portapapeis"

#: editactiondialog.cpp:122
#, kde-format
msgid "Command"
msgstr "Orde"

#: editactiondialog.cpp:124
#, kde-format
msgid "Output"
msgstr ""

#: editactiondialog.cpp:126
#, kde-format
msgid "Description"
msgstr "Descrición"

#: editactiondialog.cpp:179
#, kde-format
msgid "Action Properties"
msgstr "Propiedades da acción"

#: editactiondialog.cpp:191
#, kde-kuit-format
msgctxt "@info"
msgid ""
"An action takes effect when its <interface>match pattern</interface> matches "
"the clipboard contents. When this happens, the action's <interface>commands</"
"interface> appear in the Klipper popup menu; if one of them is chosen, the "
"command is executed."
msgstr ""

#: editactiondialog.cpp:203
#, kde-format
msgid "Enter a pattern to match against the clipboard"
msgstr ""

#: editactiondialog.cpp:205
#, kde-format
msgid "Match pattern:"
msgstr ""

#: editactiondialog.cpp:208
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The match pattern is a regular expression. For more information see the "
"<link url=\"https://en.wikipedia.org/wiki/Regular_expression\">Wikipedia "
"entry</link> for this topic."
msgstr ""

#: editactiondialog.cpp:219
#, kde-format
msgid "Enter a description for the action"
msgstr ""

#: editactiondialog.cpp:220 editcommanddialog.cpp:83
#, kde-format
msgid "Description:"
msgstr "Descrición:"

#: editactiondialog.cpp:223
#, kde-format
msgid "Include in automatic popup"
msgstr ""

#: editactiondialog.cpp:227
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The commands for this match will be included in the automatic action popup, "
"if it is enabled in the <interface>Action Menu</interface> page. If this "
"option is turned off, the commands for this match will not be included in "
"the automatic popup but they will be included if the popup is activated "
"manually with the <shortcut>%1</shortcut> key shortcut."
msgstr ""

#: editactiondialog.cpp:262
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Add Command..."
msgstr "Engadir unha orde"

#: editactiondialog.cpp:267
#, fuzzy, kde-format
#| msgid "Add Command"
msgid "Edit Command..."
msgstr "Engadir unha orde"

#: editactiondialog.cpp:273
#, fuzzy, kde-format
#| msgid "Remove Command"
msgid "Delete Command"
msgstr "Retirar a orde"

#: editactiondialog.cpp:388
#, kde-kuit-format
msgctxt "@info"
msgid "Delete the selected command <resource>%1</resource>?"
msgstr ""

#: editactiondialog.cpp:389
#, kde-format
msgid "Confirm Delete Command"
msgstr ""

#: editcommanddialog.cpp:46
#, fuzzy, kde-format
#| msgid "Action Properties"
msgid "Command Properties"
msgstr "Propiedades da acción"

#: editcommanddialog.cpp:59
#, kde-format
msgid "Enter the command and arguments"
msgstr ""

#: editcommanddialog.cpp:62
#, fuzzy, kde-format
#| msgid "Command"
msgid "Command:"
msgstr "Orde"

#: editcommanddialog.cpp:71
#, kde-kuit-format
msgctxt "@info"
msgid ""
"A <placeholder>&#37;s</placeholder> in the command will be replaced by the "
"complete clipboard contents. <placeholder>&#37;0</placeholder> through "
"<placeholder>&#37;9</placeholder> will be replaced by the corresponding "
"captured texts from the match pattern."
msgstr ""

#: editcommanddialog.cpp:81
#, kde-format
msgid "Enter a description for the command"
msgstr ""

#: editcommanddialog.cpp:91
#, kde-format
msgid "Output from command:"
msgstr ""

#: editcommanddialog.cpp:93
#, fuzzy, kde-format
#| msgid "Replace Clipboard"
msgid "Replace current clipboard"
msgstr "Substituír o portapapeis"

#: editcommanddialog.cpp:97
#, fuzzy, kde-format
#| msgid "Add to Clipboard"
msgid "Append to clipboard"
msgstr "Engadir ao portapapeis"

#: editcommanddialog.cpp:101
#, kde-format
msgid "What happens to the standard output of the command executed."
msgstr ""

#: editcommanddialog.cpp:115
#, kde-format
msgid "Reset the icon to the default for the command"
msgstr ""

#: editcommanddialog.cpp:121
#, kde-format
msgid "Icon:"
msgstr ""

#: historyimageitem.cpp:39
#, kde-format
msgid "%1x%2 %3bpp"
msgstr ""

#: klipper.cpp:154
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgctxt "@action:inmenu Toggle automatic action"
msgid "Automatic Action Popup Menu"
msgstr "Tempo de agarda para xanelas emerxentes de acción:"

#: klipper.cpp:177
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgctxt "@action:inmenu"
msgid "C&lear Clipboard History"
msgstr "Ba&leirar o historial do portapapeis"

#: klipper.cpp:184
#, fuzzy, kde-format
#| msgid "&Configure Klipper..."
msgctxt "@action:inmenu"
msgid "&Configure Klipper…"
msgstr "&Configurar Klipper…"

#: klipper.cpp:190
#, fuzzy, kde-format
#| msgctxt "@item:inmenu Quit Klipper"
#| msgid "&Quit"
msgctxt "@action:inmenu Quit Klipper"
msgid "&Quit"
msgstr "&Saír"

#: klipper.cpp:195
#, fuzzy, kde-format
#| msgid "Manually Invoke Action on Current Clipboard"
msgctxt "@action:inmenu"
msgid "Manually Invoke Action on Current Clipboard"
msgstr "Invocar manualmente a acción no portapapeis actual"

#: klipper.cpp:203
#, fuzzy, kde-format
#| msgid "Edit Contents"
msgctxt "@action:inmenu"
msgid "&Edit Contents…"
msgstr "Editar o contido"

#: klipper.cpp:211
#, fuzzy, kde-format
#| msgid "&Show Barcode..."
msgctxt "@action:inmenu"
msgid "&Show Barcode…"
msgstr "&Mostrar o código de barras…"

#: klipper.cpp:220
#, fuzzy, kde-format
#| msgid "Next History Item"
msgctxt "@action:inmenu"
msgid "Next History Item"
msgstr "Seguinte elemento do historial"

#: klipper.cpp:225
#, fuzzy, kde-format
#| msgid "Previous History Item"
msgctxt "@action:inmenu"
msgid "Previous History Item"
msgstr "Anterior elemento do historial"

#: klipper.cpp:232
#, fuzzy, kde-format
#| msgid "Open Klipper at Mouse Position"
msgctxt "@action:inmenu"
msgid "Show Items at Mouse Position"
msgstr "Abrir Klipper na posición do rato"

#: klipper.cpp:243
#, fuzzy, kde-format
#| msgid "Klipper - Clipboard Tool"
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Tool"
msgstr "Klipper, a utilidade de portapapeis"

#: klipper.cpp:556
#, kde-kuit-format
msgctxt "@info"
msgid ""
"You can enable URL actions later in the <interface>Actions</interface> page "
"of the Clipboard applet's configuration window"
msgstr ""

#: klipper.cpp:595
#, kde-format
msgid "Should Klipper start automatically when you login?"
msgstr "Debería iniciarse automaticamente Klipper ao acceder?"

#: klipper.cpp:596
#, kde-format
msgid "Automatically Start Klipper?"
msgstr "Desexa iniciar Klipper automaticamente?"

#: klipper.cpp:597
#, kde-format
msgid "Start"
msgstr "Iniciar"

#: klipper.cpp:598
#, kde-format
msgid "Do Not Start"
msgstr "Non iniciar"

#: klipper.cpp:945
#, kde-format
msgid "Edit Contents"
msgstr "Editar o contido"

#: klipper.cpp:1012
#, kde-format
msgid "Mobile Barcode"
msgstr "Código de barras de móbil"

#: klipper.cpp:1059
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid "Do you really want to clear and delete the entire clipboard history?"
msgstr "Está seguro de que quere eliminar todo o historial do portapapeis?"

#: klipper.cpp:1060
#, fuzzy, kde-format
#| msgid "C&lear Clipboard History"
msgid "Clear Clipboard History"
msgstr "Ba&leirar o historial do portapapeis"

#: klipper.cpp:1076 klipper.cpp:1085
#, kde-format
msgid "Clipboard history"
msgstr "Historial do portapapeis"

#: klipper.cpp:1102
#, kde-format
msgid "up"
msgstr "arriba"

#: klipper.cpp:1109
#, kde-format
msgid "current"
msgstr "actual"

#: klipper.cpp:1116
#, kde-format
msgid "down"
msgstr "abaixo"

#. i18n: ectx: label, entry (Version), group (General)
#: klipper.kcfg:10
#, kde-format
msgid "Klipper version"
msgstr "Versión do Klipper"

#. i18n: ectx: label, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:13
#, kde-format
msgid "Save history across desktop sessions"
msgstr ""

#. i18n: ectx: tooltip, entry (KeepClipboardContents), group (General)
#: klipper.kcfg:15
#, kde-format
msgid ""
"Retain the clipboard history, so it will be available the next time you log "
"in."
msgstr ""

#. i18n: ectx: label, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:18
#, fuzzy, kde-format
#| msgid "Prevent empty clipboard"
msgid "Prevent the clipboard from being cleared"
msgstr "Evitar ter o portapapeis baleiro"

#. i18n: ectx: whatsthis, entry (PreventEmptyClipboard), group (General)
#: klipper.kcfg:20
#, kde-format
msgid ""
"Do not allow the clipboard to be cleared, for example when an application "
"exits."
msgstr ""

#. i18n: ectx: label, entry (SyncClipboards), group (General)
#: klipper.kcfg:27
#, fuzzy, kde-format
#| msgid "Selection and Clipboard"
msgid "Keep the selection and clipboard the same"
msgstr "Selección e portapapeis"

#. i18n: ectx: whatsthis, entry (SyncClipboards), group (General)
#: klipper.kcfg:29
#, kde-format
msgid ""
"Content selected with the cursor is automatically copied to the clipboard so "
"that it can be pasted with either a Paste action or a middle-click.<br/><a "
"href=\"1\">More about the selection and clipboard</a>."
msgstr ""

#. i18n: ectx: label, entry (IgnoreSelection), group (General)
#: klipper.kcfg:32
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Ignore the selection"
msgstr "Ignorar a selección"

#. i18n: ectx: whatsthis, entry (IgnoreSelection), group (General)
#: klipper.kcfg:34
#, kde-format
msgid ""
"Content selected with the cursor but not explicitly copied to the clipboard "
"is not automatically stored in the clipboard history, and can only be pasted "
"using a middle-click."
msgstr ""

#. i18n: ectx: label, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:37
#, kde-format
msgid "Text selection only"
msgstr "Só o texto da selección"

#. i18n: ectx: whatsthis, entry (SelectionTextOnly), group (General)
#: klipper.kcfg:39
#, kde-format
msgid ""
"Only store text selections in the clipboard history, not images or any other "
"type of data."
msgstr ""

#. i18n: ectx: label, entry (IgnoreImages), group (General)
#: klipper.kcfg:42
#, fuzzy, kde-format
#| msgid "Ignore images"
msgid "Always ignore images"
msgstr "Ignorar as imaxes"

#. i18n: ectx: whatsthis, entry (IgnoreImages), group (General)
#: klipper.kcfg:44
#, kde-format
msgid ""
"Do not store images in the clipboard history, even if explicitly copied."
msgstr ""

#. i18n: ectx: label, entry (UseGUIRegExpEditor), group (General)
#: klipper.kcfg:47
#, kde-format
msgid "Use graphical regexp editor"
msgstr "Empregar un editor gráfico de expresións regulares"

#. i18n: ectx: label, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:51
#, fuzzy, kde-format
#| msgid "Ignore selection"
msgid "Immediately on selection"
msgstr "Ignorar a selección"

#. i18n: ectx: tooltip, entry (URLGrabberEnabled), group (General)
#: klipper.kcfg:52
#, kde-format
msgid ""
"Show the popup menu of applicable actions as soon as a selection is made."
msgstr ""

#. i18n: ectx: label, entry (NoActionsForWM_CLASS), group (General)
#: klipper.kcfg:57
#, kde-format
msgid "No actions for WM_CLASS"
msgstr "Non hai ningunha acción para a WM_CLASS"

#. i18n: ectx: label, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:60
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Automatic action menu time:"
msgstr "Tempo de agarda para xanelas emerxentes de acción:"

#. i18n: ectx: tooltip, entry (TimeoutForActionPopups), group (General)
#: klipper.kcfg:64
#, fuzzy, kde-format
#| msgid "Timeout for action popups:"
msgid "Display the automatic action popup menu for this time."
msgstr "Tempo de agarda para xanelas emerxentes de acción:"

#. i18n: ectx: label, entry (MaxClipItems), group (General)
#: klipper.kcfg:67
#, fuzzy, kde-format
#| msgid "Clipboard history size:"
msgid "History size:"
msgstr "Tamaño do historial do portapapeis:"

#. i18n: ectx: tooltip, entry (MaxClipItems), group (General)
#: klipper.kcfg:71
#, kde-format
msgid "The clipboard history will store up to this many items."
msgstr ""

#. i18n: ectx: label, entry (ActionList), group (General)
#: klipper.kcfg:74
#, kde-format
msgid "Dummy entry for indicating changes in an action's tree widget"
msgstr ""
"Entrada de probas para indicar cambios nun trebello de árbore de accións"

#. i18n: ectx: label, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:84
#, fuzzy, kde-format
#| msgid "Strip whitespace when executing an action"
msgid "Trim whitespace from selection"
msgstr "Quitar o espazo en branco cando se execute algunha acción"

#. i18n: ectx: whatsthis, entry (StripWhiteSpace), group (Actions)
#: klipper.kcfg:86
#, kde-format
msgid ""
"Remove any whitespace from the start and end of selected text, before "
"performing an action. For example, this ensures that a URL pasted in a "
"browser is interpreted as expected. The text saved on the clipboard is not "
"affected."
msgstr ""

#. i18n: ectx: label, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:89
#, fuzzy, kde-format
#| msgid "Replay actions on an item selected from history"
msgid "For an item chosen from history"
msgstr "Reproducir as accións nun elemento escollido do historial"

#. i18n: ectx: tooltip, entry (ReplayActionInHistory), group (Actions)
#: klipper.kcfg:91
#, fuzzy, kde-format
#| msgid "Really delete entire clipboard history?"
msgid ""
"Show the popup menu of applicable actions if an entry is chosen from the "
"clipboard history."
msgstr "Está seguro de que quere eliminar todo o historial do portapapeis?"

#. i18n: ectx: label, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:94
#, fuzzy, kde-format
#| msgid "Enable MIME-based actions"
msgid "Include MIME actions"
msgstr "Activar as accións baseadas no tipo MIME"

#. i18n: ectx: whatsthis, entry (EnableMagicMimeActions), group (Actions)
#: klipper.kcfg:96
#, kde-format
msgid ""
"If a file name or URL is selected, include applications that can accept its "
"MIME type in the popup menu."
msgstr ""

#: klipperpopup.cpp:105
#, fuzzy, kde-format
#| msgid "Clipboard Contents"
msgctxt "%1 is application display name"
msgid "%1 - Clipboard Items"
msgstr "Contido do portapapeis"

#: klipperpopup.cpp:109
#, fuzzy, kde-format
#| msgid "Search..."
msgid "Search…"
msgstr "Buscar…"

#: klipperpopup.cpp:167
#, fuzzy, kde-format
#| msgid "Regular expression:"
msgid "Invalid regular expression, %1"
msgstr "Expresión regular:"

#: klipperpopup.cpp:172 tray.cpp:25 tray.cpp:53
#, kde-format
msgid "Clipboard is empty"
msgstr "O portapapeis está baleiro"

#: klipperpopup.cpp:174
#, fuzzy, kde-format
#| msgid "<no matches>"
msgid "No matches"
msgstr "<sen coincidencias>"

#: main.cpp:27 tray.cpp:22
#, kde-format
msgid "Klipper"
msgstr "Klipper"

#: main.cpp:29
#, fuzzy, kde-format
#| msgid "KDE cut & paste history utility"
msgid "Plasma cut & paste history utility"
msgstr "Utilidade do historial de cortar e pegar de KDE"

#: main.cpp:31
#, kde-format
msgid ""
"(c) 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"
msgstr ""
"© 1998, Andrew Stanley-Jones\n"
"1998-2002, Carsten Pfeiffer\n"
"2001, Patrick Dubroy"

#: main.cpp:34
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:34
#, kde-format
msgid "Author"
msgstr "Autor"

#: main.cpp:36
#, kde-format
msgid "Andrew Stanley-Jones"
msgstr "Andrew Stanley-Jones"

#: main.cpp:36
#, kde-format
msgid "Original Author"
msgstr "Autor orixinal"

#: main.cpp:38
#, kde-format
msgid "Patrick Dubroy"
msgstr "Patrick Dubroy"

#: main.cpp:38
#, kde-format
msgid "Contributor"
msgstr "Colaborador"

#: main.cpp:40
#, kde-format
msgid "Luboš Luňák"
msgstr "Luboš Luňák"

#: main.cpp:40
#, kde-format
msgid "Bugfixes and optimizations"
msgstr "Correccións e optimizacións"

#: main.cpp:42
#, kde-format
msgid "Esben Mose Hansen"
msgstr "Esben Mose Hansen"

#: main.cpp:42
#, kde-format
msgid "Previous Maintainer"
msgstr "Anterior mantenedor."

#: main.cpp:44
#, kde-format
msgid "Martin Gräßlin"
msgstr "Martin Gräßlin"

#: main.cpp:44
#, kde-format
msgid "Maintainer"
msgstr "Mantenedor"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Xabi García, Jesús Bravo, Marce Villarino"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "xabigf@gmx.net, jba@pobox.com, mvillarino@users.sourceforge.net"

#: popupproxy.cpp:145
#, kde-format
msgid "&More"
msgstr "&Máis"

#: tray.cpp:25
#, kde-format
msgid "Clipboard Contents"
msgstr "Contido do portapapeis"

#: urlgrabber.cpp:199
#, kde-format
msgid "Disable This Popup"
msgstr "Desactivar esta xanela emerxente"

#: urlgrabber.cpp:205
#, kde-format
msgid "&Cancel"
msgstr "&Cancelar"

#~ msgid "Delete clipboard history?"
#~ msgstr "Quere eliminar o historial do portapapeis?"

#~ msgid "Action list:"
#~ msgstr "Lista de accións:"

#~ msgid "Regular Expression"
#~ msgstr "Expresión regular"

#, fuzzy
#~| msgid ""
#~| "Click on a highlighted item's column to change it. \"%s\" in a command "
#~| "will be replaced with the clipboard contents.<br>For more information "
#~| "about regular expressions, you could have a look at the <a href=\"http://"
#~| "en.wikipedia.org/wiki/Regular_expression\">Wikipedia entry about this "
#~| "topic</a>."
#~ msgid ""
#~ "Click on a highlighted item's column to change it. \"%s\" in a command "
#~ "will be replaced with the clipboard contents.<br>For more information "
#~ "about regular expressions, you could have a look at the <a href=\"https://"
#~ "en.wikipedia.org/wiki/Regular_expression\">Wikipedia entry about this "
#~ "topic</a>."
#~ msgstr ""
#~ "Prema a columna dun elemento realzado para cambiala. Cando apareza «%s» "
#~ "nunha orde substituirase polo contido do portapapeis.<br>Para máis "
#~ "información sobre as expresións regulares, consulte o <a href=\"http://en."
#~ "wikipedia.org/wiki/Regular_expression\">artículo de Wikipedia sobre este "
#~ "tema</a>."

#~ msgid "Output Handling"
#~ msgstr "Xestión da saída"

#~ msgid "new command"
#~ msgstr "nova orde"

#~ msgid "Command Description"
#~ msgstr "Descrición da orde"

#~ msgid "Action properties:"
#~ msgstr "Propiedades da acción:"

#~ msgid "Automatic:"
#~ msgstr "Automático:"

#~ msgid "List of commands for this action:"
#~ msgstr "Lista de ordes desta acción:"

#~ msgid "Double-click an item to edit"
#~ msgstr "Faga duplo-clic nun elemento para editalo"

#~ msgid "Remove whitespace when executing actions"
#~ msgstr "Retirar os espazos en branco cando se executen accións"

#~ msgid "Advanced..."
#~ msgstr "Avanzado…"

#~ msgid "Advanced Settings"
#~ msgstr "Configuración avanzada"

#~ msgid "D&isable Actions for Windows of Type WM_CLASS"
#~ msgstr "Desact&ivar as accións para as xanelas do tipo WM_CLASS"

#~ msgid ""
#~ "<qt>This lets you specify windows in which Klipper should not invoke "
#~ "\"actions\". Use<br /><br /><center><b>xprop | grep WM_CLASS</b></"
#~ "center><br />in a terminal to find out the WM_CLASS of a window. Next, "
#~ "click on the window you want to examine. The first string it outputs "
#~ "after the equal sign is the one you need to enter here.</qt>"
#~ msgstr ""
#~ "<qt>Isto permítelle especificar as xanelas nas que Klipper non debe "
#~ "invocar «accións». Use <br/><br/><center><b>xprop | grep WM_CLASS</b></"
#~ "center> <br/>nunha terminal para descubrir a WM_CLASS dunha xanela. "
#~ "Seguidamente, prema a xanela que quere examinar. A primeira cadea que "
#~ "aparece despois do signo de igualdade é a que precisa inserir aquí.</qt>"

#~ msgid "Enable Clipboard Actions"
#~ msgstr "Activar as accións do portapapeis"

#~ msgid "URL grabber enabled"
#~ msgstr "Activouse o capturador de URL"

#~ msgid "Replay action in history"
#~ msgstr "Reproducir a acción do historial"

#~ msgid "Save clipboard contents on exit"
#~ msgstr "Gardar o contido do portapapeis ao saír"

#~ msgid "Synchronize contents of the clipboard and the selection"
#~ msgstr "Sincronizar o contido do portapapeis e a selección"

#~ msgid "Keep clipboard contents"
#~ msgstr "Conservar o contido do portapapeis"

#~ msgid ""
#~ "Selecting this option has the effect, that the clipboard can never be "
#~ "emptied. E.g. when an application exits, the clipboard would usually be "
#~ "emptied."
#~ msgstr ""
#~ "Se sinala esta opción terá o efecto de que o portapapeis nunca pode "
#~ "baleirarse. P. ex. cando unha aplicación sae, o portapapeis normalmente "
#~ "baléirase."

#~ msgid "Ignore Selection"
#~ msgstr "Ignorar a selección"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is set, the selection is not "
#~ "entered into the clipboard history, though it is still available for "
#~ "pasting using the middle mouse button."
#~ msgstr ""
#~ "A área da pantalla escollida co rato ou o teclado chámase «a escolla».<br/"
#~ ">Se define esta opción, non se inserirá a selección no historial do "
#~ "portapapeis, aínda que as ha poder pegar mediante o botón central do rato."

#~ msgid "Synchronize clipboard and selection"
#~ msgstr "Sincronizar o portapapeis e a selección"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, the selection "
#~ "and the clipboard is kept the same, so that anything in the selection is "
#~ "immediately available for pasting elsewhere using any method, including "
#~ "the traditional middle mouse button. Otherwise, the selection is recorded "
#~ "in the clipboard history, but the selection can only be pasted using the "
#~ "middle mouse button. Also see the 'Ignore Selection' option."
#~ msgstr ""
#~ "Cando escolle unha área da pantalla co rato ou o teclado, iso chámase «a "
#~ "escolla».<br/>Se escolle esta opción a escolla e o portapapeis mantéñense "
#~ "iguais, polo que calquera cousa que sexa a escolla estará dispoñíbel "
#~ "inmediatamente para apegarse en calquera outro sitio empregando calquera "
#~ "zmétodo, incluíndo o botón central do rato. Noutro caso a escolla "
#~ "rexistrarase no historial do portapapeis pero só se poderá apegar co "
#~ "botón central do rato. Consulte tamén a opción Ignorar a escolla."

#~ msgid "Selection text only"
#~ msgstr "Só o texto da selección"

#~ msgid ""
#~ "When an area of the screen is selected with mouse or keyboard, this is "
#~ "called \"the selection\".<br/>If this option is selected, only text "
#~ "selections are stored in the history, while images and other selections "
#~ "are not."
#~ msgstr ""
#~ "Cando se escolle unha área da pantalla co rato ou o teclado chámaselle «a "
#~ "escolla»<br/>. Se escolle esta opción, só se gardarán as escollas de "
#~ "texto no historial, as imaxes e outras escollas non se gardarán."

#~ msgid "Timeout for action popups (seconds)"
#~ msgstr "Tempo de agarda para xanelas emerxentes de acción (segundos)"

#~ msgid "A value of 0 disables the timeout"
#~ msgstr "Un valor de 0 desactiva o tempo de agarda"

#~ msgid "Clipboard history size"
#~ msgstr "Tamaño do historial do portapapeis"

#~ msgid ""
#~ "Sometimes, the selected text has some whitespace at the end, which, if "
#~ "loaded as URL in a browser would cause an error. Enabling this option "
#~ "removes any whitespace at the beginning or end of the selected string "
#~ "(the original clipboard contents will not be modified)."
#~ msgstr ""
#~ "Algunhas veces, o texto escollido ten algún espazo en branco ao remate, o "
#~ "cal, se é usado como URL nun navegador, ocasionará un erro. Ao activar "
#~ "esta opción retirará calquera espazo en branco ao comezo e remate da "
#~ "cadea escollida (o contido orixinal do portapapeis non se modificará)."

#~ msgid "%1 - Actions For: %2"
#~ msgstr "%1 - Accións para: %2"

#~ msgid "&Edit Contents..."
#~ msgstr "&Editar o contido…"

#~ msgid "<empty clipboard>"
#~ msgstr "<portapapeis baleiro>"

#~ msgid ""
#~ "You can enable URL actions later by left-clicking on the Klipper icon and "
#~ "selecting 'Enable Clipboard Actions'"
#~ msgstr ""
#~ "Pode activar máis tarde as accións de URL premendo co botón esquerdo do "
#~ "rato na icona de Klipper e escollendo «Activar as accións do portapapeis»"

#~ msgid "Enable Clipboard &Actions"
#~ msgstr "Activar as &accións do portarretallos"
